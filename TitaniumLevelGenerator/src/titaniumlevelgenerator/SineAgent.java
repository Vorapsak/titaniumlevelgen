package titaniumlevelgenerator;

/**
 * A simple example Agent, mostly exists for making sure the procedure is working.
 * @author Jeff
 */
class SineAgent extends Agent {

    @Override
    public void work(int[][] level) {
        for(int i = 0; i < level.length; i++){
            for(int j = 0; j < (Math.sin(i)+1)*(level[0].length/2-1); j++){
                level[i][j] = 1;
            }
        }
    }
}
