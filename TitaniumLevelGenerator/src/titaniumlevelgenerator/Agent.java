/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package titaniumlevelgenerator;

/**
 *
 * @author Jeff
 */
abstract class Agent {
    private int xcenter, ycenter, xrange, yrange;
    
    public Agent(){
    }
    
    public void setDomain(int xc, int yc, int xr, int yr){
        xcenter = xc;
        ycenter = yc;
        xrange = xr;
        yrange = yr;
        
    }
    
    abstract public void work(int[][] level);
}
