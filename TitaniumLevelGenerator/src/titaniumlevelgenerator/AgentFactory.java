package titaniumlevelgenerator;

import java.util.Random;

/**
 *
 * @author Jeff
 */
class AgentFactory{
    static Random rand = new Random();
    
    public Agent getRandomAgent(){
        return new SineAgent();
    }
}
