/*
 * The main class for Titanium's Level Generation package. Simply executing the
 * JAR file will output Level.txt in the directory it was run from, using default
 * generation settings.
 */
package titaniumlevelgenerator;

import java.io.FileWriter;
import java.util.ArrayList;

/**
 *
 * @author Jeff L'Heureux
 */
public class TitaniumLevelGenerator {
    private ArrayList<Agent> agents;
    
    
    public TitaniumLevelGenerator(){
        this(1);
    }
    
    public TitaniumLevelGenerator(int numAgents){
        agents = new ArrayList<Agent>();
        AgentFactory generator = new AgentFactory();
        for(int i=0;i<numAgents;i++){
            agents.add(generator.getRandomAgent());
        }
    }
    
    public static void main(String[] args) {
        new TitaniumLevelGenerator().run();
    }

    private void run(){
        this.run("Level.txt");
    }

    private void run(String outfile) {
        this.run(outfile, 100, 100);
    }
    
    public void run(String outfile, int width, int height){
        int[][] level = new int[width][height];
        
        //Set the domains of each agent.
        
        //Generate!
        for (Agent a : agents){
            a.work(level);
        }
        
        //Record.
        writeOut(level, outfile);
        
    }

    private void writeOut(int[][] level, String outfile) {
        try{
            FileWriter fw = new FileWriter(outfile);
            
            //do the actual writing in proper format
            int chunkNum = 1; //one-indexed, I dunno why
            //Write one chunk
            fw.write('{');
            //metadata
            fw.write("\n\"chunkNumber\":" + chunkNum + ",");
            fw.write("\n\"chunkSize\":" + level.length*level[0].length + ",");
            //the array itself
            fw.write("\n\"data\":[");
            
            //first row
            fw.write("\n[{\"type\":" + level[0][0] + "}");
            for(int j = 1; j < level[0].length; j++){
                fw.write(", {\"type\":" + level[0][j] + "}");
            }
            fw.write("]");
            //yep that was a fencepost.
            for(int i = 1; i < level.length; i++){
                fw.write(",\n[{\"type\":" + level[i][0] + "}");
                for(int j = 1; j < level[i].length; j++){
                    fw.write(", {\"type\":" + level[i][j] + "}");
                }
                fw.write("]");
            }
            fw.write("]\n}");
            //finished a chunk
            
            //don't forget to close the writer!
            fw.close();

        } catch (Exception e){
            System.out.println("Failed to create or write to file");
        }
    }
}
